
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class OX {
    
    static char table[][] = {{'-','-','-'}, {'-','-','-'}, {'-','-','-'}};
    static char currentTurn ='O';
    static int row, column;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int countTurn = 1;
    
    public static void main(String[] args) {
        
        printWelcome();
        showTable();
        
        while(!finish && countTurn <= 9){
            
            showTurn();
            process();
            showTable();
            
            if(finish)
                break;
        }
        
        showResult(finish, currentTurn);
    }

    public static void printWelcome() {
        System.out.println("Welcome to OX Game");
    }
    
    public static void showTable(){
        for(int r = 0; r < table.length; r++){
                    for(int c = 0; c < table[r].length; c++)
                         System.out.print(table[r][c]+"    ");
                    System.out.println();
        }
    }

    public static void showTurn() {
        System.out.println("Turn "+currentTurn);
    }

    public static void inputRowCol() {
        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        column = kb.nextInt();
    }

    public static void process() {
        inputRowCol();
        System.out.println("");
        if(setTable()){
            if(checkWin(table, column, row, currentTurn)){
                finish = true;
            } else {
                countTurn++;
                switchTurn();
            }
       
        } else {
            process();
         }            
    }

    public static boolean setTable() {
        if(table[row-1][column-1] == '-'){
            table[row-1][column-1] = currentTurn;
            return true;
        } else {
            System.out.println("Error!! This position can not input");
            return false;
        }
    }

    public static void switchTurn() {
        if(currentTurn == 'O') 
            currentTurn = 'X';
        else 
            currentTurn = 'O';
    }

    public static boolean checkWin(char[][] table, int column, int row, char currentTurn) {
        if(checkColumn(table, column, currentTurn)){
            return true;
        }
        if(checkRow(table, row, currentTurn)){
            return true;            
        }
        if(checkX(table, currentTurn)){
            return true;
        }
        return false;
    }
    
     public static boolean checkColumn(char[][] table, int column, char currentTurn){
       for(int r = 0; r < table.length; r++){
           if(table[r][column-1] != currentTurn) return false;
       }
       return true; 
    }  
    
    public static boolean checkRow(char[][] table, int row, char currentTurn){
        for(int c = 0; c < table[row-1].length; c++){
           if(table[row-1][c] != currentTurn) return false;
       }
       return true; 
    }
    
    public static boolean checkX(char[][] table, char currentTurn){
        if(table[0][0] == currentTurn & table[1][1] == currentTurn & table[2][2] == currentTurn)
            return true;
        
        else if(table[0][2] == currentTurn & table[1][1] == currentTurn & table[2][0] == currentTurn)
            return true;
        
        else
            return false;
    }

    public static String showResult(boolean finish, char currentTurn) {
        if(finish)
            return ">>> "+currentTurn+" Win <<<";
        
        return ">>> Draw <<<";
    }
}
