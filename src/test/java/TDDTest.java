/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class TDDTest {
    
    public TDDTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //add(3, 4) -> 7
     @Test
     public void testAdd_3_4is7() {
         assertEquals(7, Example.add(3,4));
     }
     
     //add(20, 13) -> 33
     @Test
     public void testAdd_20_13is33() {
         assertEquals(33, Example.add(20,13));
     }
     
     // p(paper), s(scissor), h(hammer)
     //chup(char p1, char p2) ->  "p1","p2","draw" 
     //draw
     @Test
     public void testChup_P_PisDraw(){
         assertEquals("draw", Example.chup('p', 'p'));
     }
     
     @Test
     public void testChup_S_SisDraw(){
         assertEquals("draw", Example.chup('s', 's'));
     }
     
     @Test
     public void testChup_H_HisDraw(){
         assertEquals("draw", Example.chup('h', 'h'));
     }
     
     //p1
     @Test
     public void testChup_P_HisP1(){
         assertEquals("p1", Example.chup('p', 'h'));
     }
     
     @Test
     public void testChup_S_PisP1(){
         assertEquals("p1", Example.chup('s', 'p'));
     }
     
     @Test
     public void testChup_H_SisP1(){
         assertEquals("p1", Example.chup('h', 's'));
     }
     
     //p2
      @Test
     public void testChup_P_SisP2(){
         assertEquals("p2", Example.chup('p', 's'));
     }
     
     @Test
     public void testChup_S_HisP2(){
         assertEquals("p2", Example.chup('s', 'h'));
     }
     
     @Test
     public void testChup_H_PisP2(){
         assertEquals("p2", Example.chup('h', 'p'));
     }
}
