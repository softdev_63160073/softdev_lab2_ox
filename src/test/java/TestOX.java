/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class TestOX {
    
    public TestOX() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckColumn1() {
        char table[][] = {{'O', 'O', 'X'},
                                    {'O', 'X', 'O'},
                                    {'O', 'X', 'X'}};
        int column = 1;
        char currentTurn = 'O';
        assertEquals(true, OX.checkColumn(table, column, currentTurn));
    }
    
    @Test
    public void testCheckColumn2() {
        char table[][] = {{'O', 'X', '-'},
                                    {'X', 'X', 'O'},
                                    {'O', 'X', 'O'}};
        int column = 2;
        char currentTurn = 'X';
        assertEquals(true, OX.checkColumn(table, column, currentTurn));
    }
    
    @Test
    public void testCheckColumn3() {
        char table[][] = {{'X', '-', 'O'},
                                    {'X', 'X', 'O'},
                                    {'O', 'X', 'O'}};
        int column = 3;
        char currentTurn = 'O';
        assertEquals(true, OX.checkColumn(table, column, currentTurn));
    }
    
    @Test
    public void testCheckRow1() {
        char table[][] = {{'O', 'O', 'O'},
                                    {'X', '-', 'X'},
                                    {'-', '-', '-'}};
        int row = 1;
        char currentTurn = 'O';
        assertEquals(true, OX.checkRow(table, row, currentTurn));
    }
    
    @Test
    public void testCheckRow2() {
        char table[][] = {{'O', 'O', 'X'},
                                    {'X', 'X', 'X'},
                                    {'O', 'O', '-'}};
        int row = 2;
        char currentTurn = 'X';
        assertEquals(true, OX.checkRow(table, row, currentTurn));
    }
    
    @Test
    public void testCheckRow3() {
        char table[][] = {{'O', 'O', 'X'},
                                    {'-', 'O', 'O'},
                                    {'X', 'X', 'X'}};
        int row = 3;
        char currentTurn = 'X';
        assertEquals(true, OX.checkRow(table, row, currentTurn));
    }
    
    @Test
    public void testCheckX1() {
        char table[][] = {{'O', 'O', 'X'},
                                    {'X', 'O', 'X'},
                                    {'O', 'X', 'O'}};
        char currentTurn = 'O';
        assertEquals(true, OX.checkX(table,currentTurn));
    }
    
    @Test
    public void testCheckX2() {
        char table[][] = {{'O', 'O', 'X'},
                                    {'O', 'X', 'X'},
                                    {'X', 'O', 'O'}};
        char currentTurn = 'X';
        assertEquals(true, OX.checkX(table,currentTurn));
    }
    
    @Test
    public void testCheckWinCol(){
        char table[][] = {{'X', '-', 'O'},
                                    {'X', 'X', 'O'},
                                    {'O', 'X', 'O'}};
        int column = 3;
        int row = 1;
        char currentTurn = 'O';
        assertEquals(true, OX.checkWin(table, column, row, currentTurn));
    }
    
    @Test
    public void testCheckWinRow(){
        char table[][] = {{'X', 'X', 'X'},
                                    {'-', '-', 'O'},
                                    {'O', '-', 'O'}};
        int column = 3;
        int row = 1;
        char currentTurn = 'X';
        assertEquals(true, OX.checkWin(table, column, row, currentTurn));
    }
    
    @Test
    public void testCheckWinX(){
        char table[][] = {{'O', 'O', 'X'},
                                    {'O', 'X', 'X'},
                                    {'X', 'O', 'O'}};;
        int column = 3;
        int row = 1;
        char currentTurn = 'X';
        assertEquals(true, OX.checkWin(table, column, row, currentTurn));
    }
    
    @Test
    public void testShowWin(){
        boolean finish = true;
        char currentTurn = 'X';
        assertEquals(">>> "+currentTurn+" Win <<<", OX.showResult(finish, currentTurn));
    }    
    
    @Test
    public void testShowDraw(){
        boolean finish = false;
        char currentTurn = 'O';
        assertEquals(">>> Draw <<<", OX.showResult(finish, currentTurn));
    }    
}
